#include <pthread.h>                            // for pthread_setname_np
#include <algorithm>                            // for max
#include <functional>                           // for function
#include <iostream>                             // for operator<<, cout, ost...
#include <utility>                              // for pair
#include "TH1.h"                                // for TH1F
#include "monsvc/MonitoringService.h"           // for MonitoringService

#include "ROSDescriptorNP/RequestManager.h"
#include "ROSDescriptorNP/RequestDescriptor.h"
#include "NPTriggerIssue.h"
#include "ers/ers.h"

using namespace ROS;

RequestManager::RequestManager(unsigned int conditionTimeout,
                               unsigned int requestTimeout,
                               unsigned int clearTimeout,
                               unsigned int maxPendingClears) :
   m_availableL1(L1_INITIAL),
   m_lateClears(0),
   m_lateRequests(0),
   m_timedOutclears(0),
   m_nTimedOut(0),
   m_nDiscards(0),
   m_requestTimeOut(requestTimeout),
   m_conditionTimeout(conditionTimeout),
   m_clearTimeout(clearTimeout),
   m_maxPendingClears(maxPendingClears){

#ifdef MONITORING
   monsvc::MonitoringService& monitoringService(monsvc::MonitoringService::instance());
   m_requestDelayHistogram=
      monitoringService.register_object(
         "/EXPERT/requestDelay",
         new TH1F("requestDelay", "Data Request delay (#mu s);delay #mu s",
                  500, 0, float(m_requestTimeOut)*1.5),
         true);
   m_clearDelayHistogram=
      monitoringService.register_object(
         "/EXPERT/clearDelay",
         new TH1F("clearDelay", "Clear delay (#mu s);delay #mu s",
                  500, 0, float(m_clearTimeout)*1.5),
         true);
#endif
}

RequestManager::~RequestManager() {
#ifdef MONITORING
   monsvc::MonitoringService& monitoringService(monsvc::MonitoringService::instance());

   m_requestDelayHistogram.lock();
   TH1F* hist=m_requestDelayHistogram.get();
   monitoringService.remove_object(hist);
   m_requestDelayHistogram.unlock();

   m_clearDelayHistogram.lock();
   hist=m_clearDelayHistogram.get();
   monitoringService.remove_object(hist);
   m_clearDelayHistogram.unlock();
#endif
}

void RequestManager::start() {
   m_nTimedOut=0;
   m_lateRequests=0;
   m_lateClears=0;
   m_timedOutclears=0;
   m_availableL1=L1_INITIAL;
   m_runActive=true;

#ifdef MONITORING
   m_requestDelayHistogram->Reset();
   m_clearDelayHistogram->Reset();
#endif
   // Make sure there's nothing still pending from last run
   m_pendingClears.clear();
   m_requests.clear();

   m_pendingThread=new std::thread(&RequestManager::pendingHandler,this);
   pthread_setname_np(m_pendingThread->native_handle(),"ReqPending");
}

void RequestManager::stop() {
   m_runActive=false;
   std::cout << "RequestManager::stop waiting for pending thread to finish\n";
   m_pendingThread->join();
   delete m_pendingThread;
   m_pendingThread=0;
   // Make sure we don't have any pending clears at start of next run
   m_pendingClears.clear();
}

bool RequestManager::checkAvailable(unsigned int l1Id) {
   unsigned int lastID=m_availableL1;
   bool result;
   if (lastID==L1_INITIAL) {
      result=false; 
   } 
   else if (lastID==0xfffffffe) {
      // Special for preloaded, always ready
      result=true;
   }
   else if (l1Id > lastID) {
      result=(l1Id>L1_MAX_WRAP && lastID<L1_MIN_WRAP);
   }
   else {
      result=!((lastID >= L1_MAX_WRAP) && (l1Id < L1_MIN_WRAP));
   }
   return result;
}

void RequestManager::addRequest(RequestDescriptor* descriptor) {
   if (checkAvailable(descriptor->level1Id())) {
      doRequest(descriptor);
   }
   else {
      std::lock_guard<std::mutex> lock(m_requestMutex);
      std::pair<std::set<Request>::iterator, bool> inserted =
         m_requests.insert(Request(descriptor->level1Id(),descriptor));
      if (!inserted.second) {
         ers::warning(NPDuplicateRequestException(ERS_HERE, descriptor->level1Id()));
         doRequest(inserted.first->descriptor());
         m_requests.erase(inserted.first);
         m_requests.insert(Request(descriptor->level1Id(),descriptor));
      }
      m_condition.notify_all();
   }
}


void RequestManager::addClears(std::vector<unsigned int>& level1IdList) {
   auto now=std::chrono::system_clock::now();
   std::vector<unsigned int> readyList;
   std::lock_guard<std::mutex> lock(m_clearMutex);
   for (unsigned int l1 : level1IdList) {
      if (checkAvailable(l1)) {
         readyList.push_back(l1);
      }
      else {
         std::pair<std::set<Clear>::iterator, bool> inserted =
            m_pendingClears.insert(Clear(l1,now));
         if (!inserted.second) {
            unsigned int age=
               std::chrono::duration_cast<std::chrono::microseconds>(now-inserted.first->m_timeStamp).count();
            ers::warning(NPDuplicateClearException(ERS_HERE, l1, age));
         }
      }
   }
   if (readyList.size()>0) {
      doClears(readyList);
   }

   // Why do I want to wake up the pending handler?
   if (readyList.size()<level1IdList.size()) {
      m_condition.notify_all();
   }
}

unsigned int RequestManager::highestValue(const std::vector<unsigned int>& l1Vec) {
   unsigned int latest=L1_INITIAL;
   unsigned int minL1=L1_INITIAL;
   unsigned int upperMinL1=L1_INITIAL;

   for (auto l1 : l1Vec) {
      if (l1==L1_INITIAL) {
         minL1=L1_INITIAL;
         break;
      }
      if (l1>L1_MAX_WRAP && l1<upperMinL1) {
         upperMinL1=l1;
      }
      if (l1<minL1) {
         minL1=l1;
      }
   }
   if (upperMinL1!=L1_INITIAL && minL1<L1_MIN_WRAP) {
      // At least one channel has not wrapped yet, use the lowest
      // value of those near the wrap point
      latest=upperMinL1;
   }
   else {
      latest=minL1;
   }
   return latest;
}

void RequestManager::pendingHandler() {
   while (m_runActive) {
      {
         std::unique_lock<std::mutex> lock(m_mutex);

         //std::cout << "Waiting for condition\n";
         m_condition.wait_for(lock, std::chrono::microseconds(m_conditionTimeout));
      }

      auto l1Vec=pollLevel1();
      unsigned int enabledChannels=l1Vec.size();
      m_availableL1=highestValue(l1Vec);

      {
         std::lock_guard<std::mutex> lock(m_clearMutex);
         auto now=std::chrono::system_clock::now();
         if (m_pendingClears.size()!=0) {
            std::vector<unsigned int> readyList;

            auto iter=m_pendingClears.begin();
            int nTimedOut=0;
            while (iter!=m_pendingClears.end()) {
               unsigned int age=
                  std::chrono::duration_cast<std::chrono::microseconds>
                  (now-iter->m_timeStamp).count();
               bool ready=false;
               bool timedOut=false;
               if (checkAvailable(iter->m_l1Id) || enabledChannels==0){
                  ready=true;
               }
               else {
                  if (age>m_clearTimeout) {
                     timedOut=true;
                     nTimedOut++;
                  }
               }
               auto deliter = iter++;
               if (ready||timedOut) {
                  readyList.push_back(deliter->m_l1Id);
                  m_pendingClears.erase(deliter);
                  m_lateClears++;
#ifdef MONITORING
                  m_clearDelayHistogram->Fill(age);
#endif
               }
            }
            if (nTimedOut>0) {
               // ers::warning(NPClearTimeoutException(ERS_HERE, 
               //                                      readyList.size(),
               //                                      nTimedOut));
               m_timedOutclears+=nTimedOut;
            }

            if (readyList.size()>0) {
               doClears(readyList);
            }
         }
      }

      {
         std::lock_guard<std::mutex> lock(m_requestMutex);
         auto now=std::chrono::system_clock::now();
         auto endIter=m_requests.end();
         auto reqIter=m_requests.begin();
         while (reqIter!=endIter) {
            RequestDescriptor* request=reqIter->descriptor();

            bool ready=checkAvailable(request->level1Id());
            if (!ready) {
               unsigned int elapsed=std::chrono::duration_cast<std::chrono::microseconds>
                  (now-request->requestTime()).count();

               if (elapsed>=m_requestTimeOut) {
//                std::cout << "request " << std::hex << request->level1Id() << std::dec
//                          << " requesting " << request->nChannels() << " channels"
//                          << " timed out after " << elapsed << "us"
//                          << " limit=" << m_requestTimeOut
//                          << " highest available=" << std::hex << m_availableL1 << std::dec << std::endl;
                  m_nTimedOut++;
                  ready=true;
               }
               if (enabledChannels==0){
                  m_nDiscards++;
                  ready=true;
               }
            }
            if (ready) {
               doDeferredRequest(request);
               auto tempIter=reqIter++;
               m_requests.erase(tempIter);
               m_lateRequests++;
#ifdef MONITORING
               unsigned int elapsed=std::chrono::duration_cast<std::chrono::microseconds>
                  (now-request->requestTime()).count();
               m_requestDelayHistogram->Fill(elapsed);
#endif
            }
            else {
               reqIter++;
            }
         }
      }
   }
}

void RequestManager::dump() {
   std::cout << "NPRequestManager::\n==================\n"
             << m_requests.size() << " pending requests:" << std::hex;
   for (auto req: m_requests) {
      std::cout << " " << req.descriptor()->level1Id();
   }
   std::cout << std::dec << std::endl;
   std::cout << m_pendingClears.size() << " pending clears:" << std::hex;
   for (auto clear : m_pendingClears) {
#if 0
      std::cout << " " << clear;
#else
      std::cout << " " << clear.m_l1Id;
#endif
   }
   std::cout << std::dec << std::endl;

   std::cout << "Highest L1 available " << std::hex << m_availableL1
             << std::dec << std::endl;
}
