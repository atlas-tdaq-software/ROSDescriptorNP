
#include <chrono>
#include <iostream>                                // for operator<<, cout
#include <list>                                    // for list
#include <utility>

#include <tbb/concurrent_queue.h>                  // for concurrent_bounded...

#include "NPTrigger.h"
#include "NPRequestManager.h"
#include "NPTriggerIssue.h"
#include "RosNPOutputMsg.h"

#include "DFdal/NPTriggerIn.h"
#include "ROSDescriptorNP/NPRequestDescriptor.h"
#include "ROSDescriptorNP/NPReadoutModule.h"

#include "DFSubSystemItem/Config.h"                // for Config, ROS
#include "NPTriggerInfo.h"                         // for NPTriggerInfo
#include "ROSCore/IOManager.h"                     // for IOManager
#include "RunControl/Common/RunControlCommands.h"

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/ClearMsg.h"

#include "asyncmsg/asyncmsg.h"

#include "ROSCore/ReadoutModule.h"

#include "dal/Partition.h"
#include "DFdal/DFParameters.h"

#include "ers/ers.h"                               // for warning, ERS_LOG
#include "monsvc/MonitoringService.h"              // for MonitoringService

#include "TH1D.h"

using namespace ROS;



NPTrigger::NPTrigger():
   m_requestManager(0),
   m_triggerActive(false),
   m_nameService(0),
   m_nIOServices(1),
   m_nDataServerThreads(1),
   m_standalone(false),
   m_ignoreClearXId(false),
   m_collectorQueue(0),
   m_monitoringService(monsvc::MonitoringService::instance())
 {
   resetCounters();
}


NPTrigger::~NPTrigger() noexcept {
   ERS_LOG ("destructor entered");
   if (m_triggerActive) {
      m_requestManager->stop();
   }
   unconfigure();
}


void NPTrigger::setup(IOManager* iom, DFCountedPointer<Config> config) {
   m_ioManager=iom;

   m_standalone=config->isKey("standalone");
}


void NPTrigger::configure(const daq::rc::TransitionCmd&) {
   const daq::df::NPTriggerIn* myDal=m_configurationDB->get<daq::df::NPTriggerIn>(m_uid);

   m_nDataServerThreads=myDal->get_nDataServerThreads();
   m_nIOServices=myDal->get_nIOServices();

   unsigned int nDescriptors=myDal->get_nDescriptors();
   unsigned int requestTimeout=myDal->get_dataRequestTimeout();
   unsigned int conditionTimeout=myDal->get_dataRequestTimeout();
   unsigned int clearTimeout=myDal->get_clearTimeout();
   m_ignoreClearXId=myDal->get_ignoreClearXId();

   for (auto module : *(m_ioManager->readoutModules())) {
      NPReadoutModule* descrModule=
         dynamic_cast<NPReadoutModule*>(module);
      if (descrModule) {
         m_readoutModules.push_back(descrModule);
      }
      else {
         ers::warning(NPModuleTypeException(ERS_HERE));
      }
   }

   NPRequestDescriptor::configure(m_readoutModules,nDescriptors);

   m_collectorQueue=new tbb::concurrent_bounded_queue<NPRequestDescriptor*>;
   m_requestManager=new NPRequestManager(m_collectorQueue,
                                         m_readoutModules,
                                         conditionTimeout,
                                         requestTimeout,
                                         clearTimeout);
#ifdef MONITORING
   float maxTime=1.5*requestTimeout;
   if (maxTime==0) {
      maxTime=500000;
   }
   m_timeHistogram=m_monitoringService.register_object("/EXPERT/requestLatency",
                                                       new TH1D("requestLatency", "Request latency (#mu s)", 500, 0, maxTime),
                                                       true);
   unsigned int nChannels=NPRequestDescriptor::nChannels();
   std::cout << "\n\n NPTrigger::setup n channels=" << nChannels << std::endl;
   float maxSize=nChannels*64000;
   m_sizeHistogram=m_monitoringService.register_object("/EXPERT/dataSize",
                                                       new TH1D("dataSize","data size (32bit words)", 500, 0, maxSize),
                                                       true);
   m_channelsHistogram=m_monitoringService.register_object("/EXPERT/nChannels",
                                                           new TH1D("nChannels",
                                                                    "Number of channels in data request",
                                                                    nChannels, 0.5, nChannels+0.5));
#endif

   const daq::df::DFParameters* dfPars=
      m_configurationDB->cast<daq::df::DFParameters>(m_partition->get_DataFlowParameters());

   if (!m_standalone) {
      // Try and get networks from OKS
      m_ipcPartition=IPCPartition(m_partition->UID().c_str());

      std::vector<std::string> networks=dfPars->get_DefaultDataNetworks();
//       std::cout << "NPTriggerIn::configure() Creating NameService\n";
      m_nameService=new daq::asyncmsg::NameService(m_ipcPartition,
                                                   networks);
   }

   std::cout << "NPTrigger::configure() opening data server\n";
   m_dataIoServices.reset(new std::vector<boost::asio::io_service>(m_nIOServices));
   m_dataServer=std::make_shared<DescriptorDataServer>(*m_dataIoServices,
                                                       m_ioManager->getName(),
                                                       m_nameService,
                                                       this);
   m_dataServer->configure(m_nDataServerThreads);
   //------>>
   int service=m_dataServer->getIoService();
   std::shared_ptr<DescriptorDataServerSession> session=std::make_shared<DescriptorDataServerSession>(
                                (*m_dataIoServices)[service],
                                this);
   m_dataServer->asyncAccept(session);
   //------>>


   std::string mcAddress=dfPars->get_MulticastAddress();
   std::string proto=mcAddress.substr(0,mcAddress.find(":"));
   if (!proto.empty()) {
      mcAddress=mcAddress.substr(mcAddress.find(":")+1);
   }

   m_clearIoService.reset(new std::vector<boost::asio::io_service>(1));
   if (mcAddress.empty() || proto=="tcp") {
      // use TCP
      m_clearServer=std::make_shared<DescriptorDataServer>(*m_clearIoService,
                                                           m_ioManager->getName(),
                                                           m_nameService,
                                                           this);
      m_clearServer->configure(1,"CLEAR_");
      m_clearServer
         ->asyncAccept(std::make_shared<DescriptorDataServerSession>(
                          (*m_clearIoService)[0],this));
      std::cout << "NPTrigger created unicast ClearSession\n";
   }
   else {
       auto sPos=mcAddress.find("/");
       std::string mcInterface;
       if (sPos!=std::string::npos) {
          mcInterface=mcAddress.substr(sPos+1);
       }
       mcAddress=mcAddress.substr(0,sPos);
       m_clearSession=std::make_shared<DescriptorClearSession>((*m_clearIoService)[0],
                                                               mcAddress,
                                                               mcInterface,
                                                               this);
       m_clearSession->asyncReceive();
       std::cout << "NPTrigger created multicast ClearSession\n";
   }

   for (auto module : m_readoutModules) {
      module->setCollectorQueue(m_collectorQueue);
   }
}

void NPTrigger::prepareForRun(const daq::rc::TransitionCmd&){
   resetCounters();
#ifdef MONITORING
   m_timeHistogram->Reset();
   m_sizeHistogram->Reset();
   m_channelsHistogram->Reset();
#endif
   m_triggerActive=true;
   m_requestManager->start();
}


void NPTrigger::unconfigure(const daq::rc::TransitionCmd&){
  unconfigure();
}
void NPTrigger::unconfigure(){
   if (m_dataServer) {
      m_dataServer->unconfigure();
      m_dataServer.reset();
   }

   if (m_dataIoServices) {
      for (unsigned int service=0;service<m_dataIoServices->size();service++) {
         m_dataIoServices->at(service).stop();
      }
   }

   if (m_clearServer) {
      m_clearServer->unconfigure();
      m_clearServer.reset();
   }

   if (m_clearSession) {
      m_clearSession->close();
      m_clearSession.reset();
   }
   if (m_clearIoService) {
      for (unsigned int service=0;service<m_clearIoService->size();service++) {
         m_clearIoService->at(service).stop();
//         m_clearIoService->at(service).reset();
      }
   }

   if (m_nameService!=0) {
      delete m_nameService;
      m_nameService=0;
   }


   if (m_requestManager!=0) {
      delete m_requestManager;
      m_requestManager=0;
   }
   if (m_collectorQueue!=0) {
      delete m_collectorQueue;
      m_collectorQueue=0;
   }

   NPRequestDescriptor::unconfigure();


   if (m_clearIoService) {
      m_clearIoService.reset();
   }

#ifdef MONITORING
   if (m_monitoringService.remove_object(m_timeHistogram.get())) {
      std::cout << "Deregistered timing histo\n";
   }
   if (m_monitoringService.remove_object(m_sizeHistogram.get())) {
      std::cout << "Deregistered size histo\n";
   }
   if (m_monitoringService.remove_object(m_channelsHistogram.get())) {
      std::cout << "Deregistered channels histo\n";
   }
#endif
}


void NPTrigger::stopGathering(const daq::rc::TransitionCmd&){
   std::cout << "NPTrigger::stopGathering: Stopping threads\n";
   m_triggerActive = false; 
   m_requestManager->stop();
}


unsigned int NPTrigger::triggerCount() {
   return m_stats.numberOfLevel1;
}


void NPTrigger::submitDataRequest(unsigned int level1Id,
                                  std::vector<unsigned int>* dataChannels,
                                  std::shared_ptr<daq::asyncmsg::Session> destination,
                                  unsigned int transactionId){
   m_requestsReceived++;
   m_totalRobsRequested+=dataChannels->size();

   NPRequestDescriptor* descriptor=NPRequestDescriptor::getDescriptor();

   descriptor->initialise(level1Id, dataChannels, destination, transactionId);
   if (descriptor->localIds()->size()!=0) {
      m_requestManager->addRequest(descriptor);
   }
   else {
      descriptor->freeResources();
   }
}


void NPTrigger::submitClearRequest(std::vector<unsigned int>& level1IdList,
                                   unsigned int transactionId) {
   if (m_clearRequestsReceived!=0 && !m_ignoreClearXId) {
      int xidIncrement=transactionId-m_lastClearXId;
      if (xidIncrement>1) {
         m_stats.clearRequestsMissed+=xidIncrement-1;
         ers::warning(NPMissedClearMessageException(ERS_HERE, xidIncrement-1));
      }
      else if ((xidIncrement < 1 && transactionId != 0) //It is not a HLTSV restart
               || (transactionId == 0 && m_lastClearXId < 10)) { //It could be a HLTSV restart
         m_stats.clearRequestsDuplicated++;
//         ers::warning(NPDuplicateClearMessageException(ERS_HERE, transactionId));
         return;
      }
   }
   m_lastClearXId=transactionId;
   m_requestManager->addClears(level1IdList);
   m_clearRequestsReceived++;
   m_stats.numberOfLevel1+=level1IdList.size();
}


void NPTrigger::finalise(NPRequestDescriptor*
#ifdef MONITORING
 descriptor
#endif
) {
#ifdef MONITORING
   auto elapsed=std::chrono::system_clock::now()-descriptor->requestTime();
   m_timeHistogram->Fill(std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count());
   m_sizeHistogram->Fill(descriptor->size());
   m_channelsHistogram->Fill(descriptor->nChannelsRequested());
#endif
}

ISInfo* NPTrigger::getISInfo() {
   m_stats.requestDescriptorsCreated=NPRequestDescriptor::descriptorsCreated();
   m_stats.requestDescriptorsFree=NPRequestDescriptor::descriptorsFree();
   m_stats.clearRequestsReceived=m_clearRequestsReceived;
   m_stats.requestsReceived=m_requestsReceived;
   m_stats.requestsPending=m_requestManager->pending();
   m_stats.requestsInProgeass=m_stats.requestDescriptorsCreated
      -m_stats.requestDescriptorsFree-m_stats.requestsPending;
   m_stats.requestsTimedOut=m_requestManager->timedOut();
   m_stats.lateRequests=m_requestManager->lateRequests();
   m_stats.clearsPending=m_requestManager->clearsPending();
   m_stats.lateClears=m_requestManager->lateClears();
   m_stats.clearsTimedOut=m_requestManager->clearsTimedOut();

   auto now=std::chrono::system_clock::now();
   auto elapsed=now-m_lastStatsTime;
   float seconds=(float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6;
   if (seconds>0) {
      float l1Interval=m_stats.numberOfLevel1-m_lastStats.numberOfLevel1;
      m_stats.level1Rate=l1Interval/seconds;
      float reqInterval=m_stats.requestsReceived-m_lastStats.requestsReceived;
      m_stats.requestRate=reqInterval/seconds;
//       std::cout << "\n\tL1 Rate " << m_stats.level1Rate << std::endl;
      if (reqInterval>0) {
         m_stats.meanRequestSize=(float)m_totalRobsRequested.exchange(0)/
            reqInterval;
      }
      else {
         m_stats.meanRequestSize=0;
      }
   }

   m_lastStats.requestsReceived=m_stats.requestsReceived;
   m_lastStats.clearRequestsMissed=m_stats.clearRequestsMissed;
   m_lastStats.numberOfLevel1=m_stats.numberOfLevel1;
   m_lastStatsTime=now;

   return &m_stats;
}

void NPTrigger::resetCounters() {
   m_lastClearXId=0;
   m_clearRequestsReceived=0;
   m_requestsReceived=0;

   m_totalRobsRequested=0;

   m_stats.clearRequestsReceived=0;
   m_stats.requestsReceived=0;
   m_stats.clearRequestsMissed=0;
   m_stats.numberOfLevel1=0;
   m_stats.clearRequestsDuplicated=0;
   m_stats.clearsTimedOut=0;

   m_lastStats.clearRequestsReceived=0;
   m_lastStats.requestsReceived=0;
   m_lastStats.clearRequestsMissed=0;
   m_lastStats.numberOfLevel1=0;

   m_lastStatsTime=std::chrono::system_clock::now();
   m_counterResetTime=std::chrono::system_clock::now();
}

void NPTrigger::user(const daq::rc::UserCmd& command) {
	if (command.commandName()=="dump") {
      m_requestManager->dump();

      std::vector<std::string> arguments=command.commandParameters();
      for (auto argv : arguments) {
         if (argv=="descriptors") {
            for (unsigned int id=0; id<NPRequestDescriptor::descriptorsCreated();id++) {
               NPRequestDescriptor* descr=NPRequestDescriptor::getDescriptor(id);
               if (descr) {
                  descr->dump();
               }
            }
         }
      }
   }
}

// ==============================================================================


DescriptorDataServer::DescriptorDataServer(std::vector<boost::asio::io_service>& ioServices,
                                           const std::string& iomName,
                                           daq::asyncmsg::NameService* nameService,
                                           NPTrigger* trigger) :
   DataServer(ioServices,iomName,nameService),m_trigger(trigger) {
}

DescriptorDataServer::~DescriptorDataServer() {
}

void DescriptorDataServer::onAccept(std::shared_ptr<daq::asyncmsg::Session> session) noexcept {
      std::lock_guard<std::mutex> lock(m_sessionsMutex);
      m_sessions.emplace_back(std::dynamic_pointer_cast<DataServerSession>(session));
      
      int server=getIoService();
      asyncAccept(std::make_shared<DescriptorDataServerSession>
               (m_ioServerService[server],m_trigger));
}



// ==============================================================================


DescriptorDataServerSession::DescriptorDataServerSession(boost::asio::io_service& ioService,
                                                         NPTrigger* trigger) :
   DataServerSession(ioService),m_trigger(trigger) {
}
DescriptorDataServerSession::~DescriptorDataServerSession() {
}


void DescriptorDataServerSession::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) {

   if (message->typeId()==DataRequestMsg::TYPE_ID) {
      std::unique_ptr<DataRequestMsg> requestMsg(dynamic_cast<DataRequestMsg*>(message.release()));
      if (requestMsg==0) {
         ers::warning(NPMsgCastException(ERS_HERE, "DataRequestMsg"));
         return;
      }

//        std::cout << std::hex
//                  << "Received request message " << requestMsg->transactionId()
//                  << " for event " << requestMsg->level1Id()
//                  << " fingering " << requestMsg->nRequestedRols() << " ROLs:";
//        std::cout << std::dec << std::endl;
      m_trigger->submitDataRequest(requestMsg->level1Id(),
                                   requestMsg->requestedRols(),
                                   shared_from_this(),
                                   requestMsg->transactionId()
         );
   }
   else if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         m_trigger->submitClearRequest(clearMsg->m_l1Ids,clearMsg->transactionId());
      }
      else {
         ers::warning(NPMsgCastException(ERS_HERE, "ClearMsg"));
      }
   }
   else {
      ers::warning(NPMsgTypeException(ERS_HERE, message->typeId()));
   }

   asyncReceive();
}

void DescriptorDataServerSession::onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>
#ifdef MONITORING
  message
#endif
) noexcept{
   //std::cout << "Sent message " << message->transactionId() << std::endl;
#ifdef MONITORING
   std::unique_ptr<const RosNPOutputMsg>
      dataMessage(dynamic_cast<const RosNPOutputMsg*>(message.release()));
   if (dataMessage) {
      m_trigger->finalise(dataMessage->m_descriptor);
   }
#endif
}

void DescriptorDataServerSession::onSendError(const boost::system::error_code& error,
                                              std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept{
   std::unique_ptr<const RosNPOutputMsg>
      dataMessage(dynamic_cast<const RosNPOutputMsg*>(message.release()));
   unsigned int l1Id=0;
   if (dataMessage) {
      l1Id=dataMessage->m_descriptor->level1Id();
   }

   ERS_LOG("Error sending message for l1Id " << std::hex << l1Id <<std::dec
           << " to " << remoteEndpoint() << ": " <<
           error.message() << " (" << error << "). Aborting.");
   asyncClose();
}

// ==============================================================================

void DescriptorClearSession::onReceive(
   std::unique_ptr<daq::asyncmsg::InputMessage> message) {
   if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         m_trigger->submitClearRequest(clearMsg->m_l1Ids,clearMsg->transactionId());
      }
      else {
         ers::warning(NPMsgCastException(ERS_HERE, "ClearMsg"));
      }
   }
   asyncReceive();
}


/** Shared library entry point */
extern "C" {
   extern TriggerIn* createNPTriggerIn();
}

TriggerIn* createNPTriggerIn(){
   return (new NPTrigger());
}
