// -*- c++ -*-
//
#ifndef ROS_NP_OUTPUT_MSG_H
#define ROS_NP_OUTPUT_MSG_H

#include "ROSasyncmsg/RosDataMsg.h"
#include "ROSDescriptorNP/NPRequestDescriptor.h"

namespace ROS {
class RosNPOutputMsg: public RosDataMsg,
                        public daq::asyncmsg::OutputMessage {
public:

   explicit RosNPOutputMsg(std::uint32_t transactionId,
                           NPRequestDescriptor* descriptor)
      : RosDataMsg(transactionId),m_descriptor(descriptor) {
   };

   virtual ~RosNPOutputMsg(){
      //std::cout << "RosNPOutputMsg destructor\n";
      m_descriptor->freeResources();
   };

   virtual std::uint32_t size() const override {
      return m_descriptor->size();
   }


   virtual void
   toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override{
      //std::cout << "RosNPOutputMsg::toBuffers()\n";
      auto channelList=m_descriptor->localIds();
      for (auto channel : *channelList) {
         auto page=m_descriptor->dataPage(channel);
         unsigned int* data=page->virtualAddress();
         std::vector<unsigned int>* header=m_descriptor->headerPage(channel);
         size_t dataSize;

         if (header->at(0)!=0) {
            unsigned int headerSize=header->at(2)*sizeof(unsigned int);
            buffers.emplace_back(header->data(),headerSize);
            dataSize=header->at(1)*sizeof(unsigned int)-headerSize;
         }
         else {
            dataSize=data[1]*sizeof(unsigned int);
         }
         buffers.emplace_back(data,dataSize);
      }
   }

   NPRequestDescriptor* m_descriptor;
};
}
#endif
