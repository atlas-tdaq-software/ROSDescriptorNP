// NPRequestDescriptor

#include <chrono>
#include <cstdint>                                  // for uint32_t
#include <exception>
#include <iomanip>                                  // for operator<<, setfill
#include <iostream>                                 // for operator<<, basic...
#include <map>                                      // for map, operator!=
#include <memory>                                   // for shared_ptr, opera...
#include <mutex>                                    // for mutex, lock_guard
#include <set>
#include <thread>
#include <utility>                                  // for move, pair
#include <vector>                                   // for vector
#
#include <tbb/concurrent_queue.h>                   // for concurrent_bounde...

#include "ROSDescriptorNP/NPRequestDescriptor.h"
#include "ROSDescriptorNP/DescriptorNPException.h"
#include "ROSDescriptorNP/NPReadoutModule.h"
#include "ROSDescriptorNP/DataPage.h"               // for DataPage
#include "ROSDescriptorNP/RequestDescriptor.h"      // for RequestDescriptor...
#include "asyncmsg/Message.h"                       // for OutputMessage
#include "asyncmsg/Session.h"                       // for Session
#include "RosNPOutputMsg.h"

#include "ROSUtilities/ROSErrorReporting.h"
#include "ers/ers.h"


using namespace ROS;

std::mutex NPRequestDescriptor::s_mutex;
uint32_t NPRequestDescriptor::s_nextId=0;
uint32_t NPRequestDescriptor::s_nDescriptors=0;
std::vector<NPRequestDescriptor*> NPRequestDescriptor::s_descriptorVector;

tbb::concurrent_bounded_queue<unsigned int> NPRequestDescriptor::s_freeQueue;

std::vector<tbb::concurrent_bounded_queue<DataPage*>* > NPRequestDescriptor::s_pageQueue;
std::vector<std::vector<uint32_t> > NPRequestDescriptor::s_channelVec;
std::map<unsigned int, unsigned int> NPRequestDescriptor::s_channelMap;
std::vector<unsigned int> NPRequestDescriptor::s_subrobMap;
std::vector<uint32_t> NPRequestDescriptor::s_idMap;


NPRequestDescriptor::NPRequestDescriptor(){
   std::lock_guard<std::mutex> lock(s_mutex);
   m_id=s_nextId;
//   std::cout << "Constructing Descriptor " << m_id << std::endl;
   s_descriptorVector.push_back(this);
   s_nextId++;
   s_nDescriptors++;

   unsigned int nChans=s_channelMap.size();
   m_headerPage.reserve(nChans);
   m_dataPage.reserve(nChans);
   for (unsigned int chan=0; chan<nChans; chan++) {
      m_headerPage.emplace_back(new std::vector<unsigned int>(16)); 
      (*m_headerPage[chan])[0]=0;
      m_dataPage.push_back(0);
   }
   m_fragmentStatus.resize(nChans);
}


// ************************************************************************
NPRequestDescriptor::~NPRequestDescriptor() {
// ************************************************************************
   for (unsigned int chan=0; chan<s_channelMap.size(); chan++) {
      delete m_headerPage[chan];
   }

}


// ************************************************************************
void NPRequestDescriptor::configure(std::vector<NPReadoutModule*> readoutModules,
                                    unsigned int nDescriptors) {
// ************************************************************************
   uint32_t baseId=0;
   for (auto module : readoutModules) {
      module->registerChannels(baseId,
                               s_pageQueue,
                               s_channelVec);
   }
   uint32_t localId=0;
   for (uint32_t subrob=0; subrob<s_pageQueue.size(); subrob++) {
      for (auto chanIter : s_channelVec[subrob]) {
         s_channelMap[chanIter]=localId;
         s_idMap.push_back(chanIter);
         s_subrobMap.push_back(subrob);
         localId++;
      }
   }

   for (unsigned int entry=0; entry<nDescriptors; entry++) {
      auto desc=new NPRequestDescriptor();
      s_freeQueue.push(desc->getId());
   }
}


void NPRequestDescriptor::unconfigure() {
   s_pageQueue.clear();
   s_channelVec.clear();

   s_freeQueue.clear();

   for (auto descr : s_descriptorVector) {
      delete descr;
   }
   s_descriptorVector.clear();

   s_channelMap.clear();
   s_subrobMap.clear();

   s_nDescriptors=0;
   s_nextId=0;
}

void NPRequestDescriptor::initialise(unsigned int level1Id,
                                     const std::vector<unsigned int>* myChannels,
                                     std::shared_ptr<daq::asyncmsg::Session> destination,
                                     unsigned int transactionId){

   m_initialiseTime=std::chrono::system_clock::now();

   m_localList.clear();

   std::set<unsigned int> channelsSeen;
   for (auto chanIter : *myChannels){
      auto mapIter=s_channelMap.find(chanIter);
      if (mapIter!=s_channelMap.end()) {
         unsigned int index=mapIter->second;
         auto inserted=channelsSeen.insert(index);
         if (inserted.second) {
            m_localList.push_back(index);
            m_fragmentStatus[index]=NEW_REQUEST;
         }
         else {
            CREATE_ROS_EXCEPTION(badChannelException, DescriptorNPException,
                                 DescriptorNPException::BAD_CHANNEL_ID,
                                 " Duplicate id=0x" << std::hex << chanIter << std::dec);
            ers::error(badChannelException);
         }
      }
      else {
         CREATE_ROS_EXCEPTION(badChannelException, DescriptorNPException,
                              DescriptorNPException::BAD_CHANNEL_ID,
                              " id=0x" << std::hex << chanIter << std::dec);
         ers::error(badChannelException);
      }
   }
   m_nChannels=m_localList.size();

   m_tries=0;

   m_level1Id=level1Id;
   m_destination=destination;
   m_transactionId=transactionId;
   m_nRepliesReceived=0;
   m_submitted=false;
   m_repliesExpected=0;
}


bool NPRequestDescriptor::complete() {
   // To be reviewed!
   //  Use condition variable?
   while (!m_submitted) {
      std::this_thread::sleep_for(std::chrono::nanoseconds(1));
   }

   return (m_nRepliesReceived==m_repliesExpected);
}

void NPRequestDescriptor::send() {
   std::unique_ptr<RosNPOutputMsg> dataMsg(
      new RosNPOutputMsg(m_transactionId,this));

   try {
      auto dest=m_destination.lock();
      if (dest) {
         dest->asyncSend(std::move(dataMsg));
      }
   }
   catch (std::exception& exc) {
      std::cerr << "NPRequestDescriptor::send caught a std::exception\n";
   }

}

unsigned int NPRequestDescriptor::size() {
   uint32_t sum=0;
   // Do sum addition
   for (auto channel : m_localList) {
      if (m_headerPage[channel]!=0) {
         sum+=m_headerPage[channel]->at(1)*sizeof(unsigned int);
      }
      else {
         auto page=m_dataPage[channel];
         unsigned int* data=page->virtualAddress();
         sum+=data[1]*sizeof(unsigned int);
      }
   }
   return sum;
}




void NPRequestDescriptor::freeResources(){
   for (auto chanIter : m_localList) {
      auto subrob=s_subrobMap[chanIter];
      s_pageQueue[subrob]->push(m_dataPage[chanIter]);
      m_dataPage[chanIter]=0;
   }

   s_freeQueue.push(m_id);
}


bool NPRequestDescriptor::channelsPending() {
// ************************************************************************
#if 1
   for (auto chanIter : m_localList) {
      if (m_fragmentStatus[chanIter]==REQUEST_PENDING) {
         return true;
      }
   }
   return false;
#else
   // Is this any clearer? I don't think so.
   return std::any_of(m_localList.begin(),m_localList.end(),
                      [this](unsigned int chan){
                         return (m_fragmentStatus[chan]==REQUEST_PENDING);});
#endif
}
bool NPRequestDescriptor::channelsCorrupt() {
// ************************************************************************
   for (auto chanIter : m_localList) {
      if (m_fragmentStatus[chanIter]==REQUEST_CORRUPT) {
         return true;
      }
   }
   return false;
}

// ******************************
void NPRequestDescriptor::dump() {
// ******************************
   auto dest=m_destination.lock();
   std::cout << "NPRequestDescriptor " << m_id << ": addr=" << this
             << " level1Id=" << m_level1Id << " (" << std::hex << m_level1Id << ")" << std::dec
      //<< ", tries=" << m_tries
             << ", destination=" << dest;
   if (dest!=0) {
      std::cout << " (" << dest->remoteEndpoint() << ")";
   }
   std::cout << ", transactionId=" << std::hex << m_transactionId << std::dec
             << ", replies:  received=" << m_nRepliesReceived
             << " expected=" << m_repliesExpected
             << ", channels=" << m_localList.size()
             << ", channel ids=";

   for (auto chanIter :m_localList) {//m_channelList) {
      std::cout << s_idMap[chanIter]
                << " (0x" << std::hex << s_idMap[chanIter] << ") "
                <<std::dec;
   }
   std::cout << std::endl;


   std::cout  << std::hex << std::setfill('0');
   for (auto chanIter : m_localList) {
      std::cout << "Channel " << chanIter
                << ", Status="  << std::setw(1) << m_fragmentStatus[chanIter];
         //<< ", header address=" << m_headerPage[chanIter];
      if (m_headerPage[chanIter]!=0) {
         std::cout << ", Header:";
         for (unsigned int word=0;word<8; word++) {
            std::cout << " " << m_headerPage[chanIter]->at(word);
         }
      }
      std::cout << ", Data:";
      if (m_dataPage[chanIter]!=0) {
         unsigned int* tPtr=m_dataPage[chanIter]->virtualAddress();
         for (unsigned int word=0;word<16; word++) {
            std::cout << " " << std::setw(8) << tPtr[word];
         }
         std::cout << "...\n";
      }
      else {
         std::cout << " NOT allocated\n";
      }
   }

   std::cout << std::dec << std::setfill(' ');
}
