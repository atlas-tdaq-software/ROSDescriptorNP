#include <pthread.h>                              // for pthread_setname_np
#include <tbb/concurrent_queue.h>                 // for concurrent_bounded_...
#include <iostream>                               // for operator<<, basic_o...

#include "monsvc/MonitoringService.h"             // for MonitoringService
#include "monsvc/ptr.h"                           // for ptr

#include "NPRequestManager.h"

#include "ROSDescriptorNP/RequestDescriptor.h"    // for RequestDescriptor
#include "ROSDescriptorNP/NPReadoutModule.h"
#include "ROSDescriptorNP/NPRequestDescriptor.h"

using namespace ROS;

NPRequestManager::NPRequestManager(tbb::concurrent_bounded_queue<NPRequestDescriptor*>* collectionQueue,
                                   std::vector<NPReadoutModule*>& modules,
                                   unsigned int conditionTimeout,
                                   unsigned int requestTimeout,
                                   unsigned int clearTimeout,
                                   unsigned int maxPendingClears)
   : RequestManager(conditionTimeout,
                    requestTimeout,clearTimeout,maxPendingClears),
     m_collectionQueue(collectionQueue),
     m_readoutModules(modules) {
   for (auto modIter : m_readoutModules) {
      modIter->getL1CountPointers(m_latestL1,m_channelEnabled);
   }

   //std::cout << "m_latestL1.size()=" << m_latestL1.size()
   //          << ", m_channelEnabled.size()=" << m_channelEnabled.size()
   //          << std::endl;

   for (unsigned int chan=0;chan<m_channelEnabled.size();chan++) {
      std::cout << "channel " << chan << " enabled=" << *m_channelEnabled[chan] << std::endl;
   }

}


NPRequestManager::~NPRequestManager() {
#ifdef MONITORING
   monsvc::MonitoringService& monitoringService(monsvc::MonitoringService::instance());
   monitoringService.remove_object(m_requestDelayHistogram.get());
   monitoringService.remove_object(m_clearDelayHistogram.get());
#endif
}


void NPRequestManager::doRequest(RequestDescriptor* descriptor) {
   NPRequestDescriptor* npDesc=dynamic_cast<NPRequestDescriptor*>(descriptor);
   for (auto module : m_readoutModules) {
      module->requestFragment(npDesc);
   }
   npDesc->submitted(true);
}


void NPRequestManager::doClears(std::vector<unsigned int>& level1IdList) {
   for (auto module : m_readoutModules) {
      module->clearFragments(level1IdList);
   }
}

void NPRequestManager::start() {
   m_collectionQueue->clear();
   m_collectorThread=new std::thread(&NPRequestManager::collector,this);
   pthread_setname_np(m_collectorThread->native_handle(),"NPCollector");
   RequestManager::start();
}
void NPRequestManager::stop() {
   m_runActive=false;
   m_collectionQueue->abort();

   std::cout << "NPRequestManager::stop waiting for collector thread to finish\n";
   if (m_collectorThread) {
      if (m_collectorThread->joinable()) {
         m_collectorThread->join();
      }
      delete m_collectorThread;
      m_collectorThread=0;
   }
   // Make sure we don't have any pending clears at start of next run
   m_pendingClears.clear();

   RequestManager::stop();

   // Make sure we don't have any pending data requests at start of next run
   for (auto reqIter : m_requests) {
      auto npDesc=dynamic_cast<NPRequestDescriptor*> (reqIter.descriptor());
      npDesc->freeResources();
   }
   m_requests.clear();
}

std::vector<unsigned int> NPRequestManager::pollLevel1(){
   std::vector<unsigned int> result;
   unsigned int enabledChannels=0;
   for (unsigned int chan=0;chan<m_latestL1.size();chan++) {
      if (*m_channelEnabled[chan]) {
         enabledChannels++;
         result.push_back(*m_latestL1[chan]);
      }
   }
   return result;
}

void NPRequestManager::collector() {
//   std::cout << "NPRequestManager data collector thread\n";

   do {
      NPRequestDescriptor* descriptor;
      try {
         m_collectionQueue->pop(descriptor);
      }
      catch (tbb::user_abort& abortException) {
         m_runActive=false;
         break;
      }

      //std::cout << "NPDataCollector::run  event " << descriptor->level1Id();
      descriptor->gotReply();
      if (descriptor->complete()) {
         if (descriptor->channelsCorrupt()) {
            doRequest(descriptor);
         }
         else {
            //std::cout << " is complete\n";
            descriptor->send();
         }
      }
      else {
         //std::cout << " not finished yet\n";
      }
   } while (m_runActive);
}


