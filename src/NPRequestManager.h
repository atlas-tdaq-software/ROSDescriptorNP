// -*- c++ -*-
#ifndef NPREQUESTMANAGER_H
#define NPREQUESTMANAGER_H

#include <thread>                            // for thread
#include <vector>                            // for vector

#include "ROSDescriptorNP/RequestManager.h"
#include "tbb/concurrent_queue.h"

namespace ROS{
   class NPReadoutModule;
   class NPRequestDescriptor;
   class RequestDescriptor;

   class NPRequestManager : public RequestManager {
   public:
      NPRequestManager(tbb::concurrent_bounded_queue<NPRequestDescriptor*>* collectionQueue,
                       std::vector<NPReadoutModule*>&,
                       unsigned int, unsigned int, unsigned int,
                       unsigned int maxPendigClears=20000);
      virtual ~NPRequestManager();
      virtual void start() override;
      virtual void stop() override;
      virtual void doClears(std::vector<unsigned int>& level1IdList) override;
      virtual std::vector<unsigned int> pollLevel1() override;
      void collector();
   private:
      virtual void doRequest(RequestDescriptor* desc) override;

      //! Input queue for
      //! RequestDescriptors that have been handled
      tbb::concurrent_bounded_queue<NPRequestDescriptor*>*  m_collectionQueue;
      std::thread* m_collectorThread;
      std::vector<NPReadoutModule*> m_readoutModules;
      std::vector<unsigned int*> m_latestL1;
      std::vector<bool*> m_channelEnabled;
   };
}
#endif
