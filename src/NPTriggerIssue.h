// -*- c++ -*-

#ifndef NPTRIGGERISSUE_H
#define NPTRIGGERISSUE_H

#include "ers/Issue.h"

ERS_DECLARE_ISSUE(ROS,
                  NPTriggerIssue,
                  " NPTrigger Issue "
                  ERS_EMPTY,
                  ERS_EMPTY
                  )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPModuleTypeException,
                       ROS::NPTriggerIssue,
                       " ReadoutApplication contains a Resource that is not an NPDescriptorReadoutModule",
                       ERS_EMPTY,
                       ERS_EMPTY
                       )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPMsgCastException,
                       ROS::NPTriggerIssue,
                       " Failed to cast message to " << what,
                       ERS_EMPTY,
                       ((const char *)what)
   )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPMsgTypeException,
                       ROS::NPTriggerIssue,
                       " Unknown message type "
                       << std::hex << what << std::dec,
                       ERS_EMPTY,
                       ((unsigned int)what)
                       )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPDuplicateRequestException,
                       ROS::NPTriggerIssue,
                       " duplicate data request for l1Id "
                       << std::hex << l1Id << std::dec,
                       ERS_EMPTY,
                       ((unsigned int)l1Id)
                       )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPDuplicateClearException,
                       ROS::NPTriggerIssue,
                       " duplicate clear request for l1Id "
                       << std::hex << l1Id << std::dec
                       << " age=" << age,
                       ERS_EMPTY,
                       ((unsigned int)l1Id)
                       ((int)age)
                       )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPDuplicateClearMessageException,
                       ROS::NPTriggerIssue,
                       " duplicate clear message for transaction Id "
                       << std::hex << xId << std::dec,
                       ERS_EMPTY,
                       ((unsigned int)xId)
                       )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPMissedClearMessageException,
                       ROS::NPTriggerIssue,
                       " missed " << nMissed << " clear messages",
                       ERS_EMPTY,
                       ((unsigned int)nMissed)
                       )

ERS_DECLARE_ISSUE_BASE(ROS,
                       NPClearTimeoutException,
                       ROS::NPTriggerIssue,
                       " submitting "
                       << nClears << " clears of which "
                       << nTimedOut << " have timed out",
                       ERS_EMPTY,
                       ((unsigned int)nClears)
                       ((unsigned int)nTimedOut)
                       )

#endif
