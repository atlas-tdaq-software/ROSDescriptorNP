// -*- c++ -*-
// $Id:$ 
//
// $Log:$
//
#ifndef NPTRIGGER_H
#define NPTRIGGER_H

#include <atomic>
#include <mutex>
#include <chrono>
#include <boost/asio/io_service.hpp>     // for io_service
#include <memory>                        // for shared_ptr, unique_ptr
#include <string>                        // for string
#include <vector>                        // for vector
#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer

#include "tbb/concurrent_queue.h"

#include "ROSCore/TriggerIn.h"

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/ClearSession.h"
#include "ROSInfo/NPTriggerInfo.h"

#include "ROSDescriptorNP/NPReadoutModule.h"
#include "ipc/partition.h"               // for IPCPartition

#ifdef MONITORING
# include "monsvc/ptr.h"
#endif

class TH1D;

namespace daq{
   namespace asyncmsg{
      class NameService;
      class Session;
   }
}
namespace monsvc {
   class MonitoringService;
}
namespace ROS{
   class NPRequestManager;
   class NPRequestDescriptor;
   class NPReadoutModule;
   class IOManager;
   class Config;

   //! ROS DataFlow Trigger

   //! Listen for 'requests' from DataFlow for data or clears
   class NPTrigger : public TriggerIn {
   public:
      NPTrigger();
      ~NPTrigger() noexcept;


      ISInfo* getISInfo() override;
      void submitDataRequest(unsigned int level1Id,
                             std::vector<unsigned int>* dataChannels,
                             std::shared_ptr<daq::asyncmsg::Session> session,
                             unsigned int transactionId);


      void submitClearRequest(std::vector<unsigned int>& level1IdList,
         unsigned int transactionId);

      void finalise(NPRequestDescriptor* descriptor);
   private:
      //! From DFThread not needed
      virtual void run() override {};
      //! From DFThread not needed
      virtual void cleanup() override {};


      //! From IOMPlugin
      virtual void configure(const daq::rc::TransitionCmd&) override;
      virtual void unconfigure(const daq::rc::TransitionCmd&) override;

      virtual void prepareForRun(const daq::rc::TransitionCmd&) override;
      virtual void setup(ROS::IOManager*, DFCountedPointer<Config>) override;
      virtual void stopGathering(const daq::rc::TransitionCmd&) override;

      virtual void user(const daq::rc::UserCmd& command) override;

      void unconfigure();

      // From TriggerIn
      unsigned int triggerCount() override;

      void resetCounters();


      NPRequestManager* m_requestManager;

      bool m_triggerActive;

      //
      std::unique_ptr<std::vector<boost::asio::io_service> > m_dataIoServices;
      std::unique_ptr<std::vector<boost::asio::io_service> >m_clearIoService;

      //! Server to field data requests, inprinciple we should
      //! have a ClearServer as well but for the moment the DataServer handles
      //! clears as well
      std::shared_ptr<DataServer> m_dataServer;
      std::shared_ptr<DataServer> m_clearServer;
      std::shared_ptr<ClearSession> m_clearSession;

      daq::asyncmsg::NameService* m_nameService;

      int m_nIOServices;
      int m_nDataServerThreads;
      IPCPartition m_ipcPartition;
      bool m_standalone;

      NPTriggerInfo m_stats;
      NPTriggerInfo m_lastStats;

      std::chrono::time_point<std::chrono::system_clock> m_counterResetTime;
      std::chrono::time_point<std::chrono::system_clock> m_lastStatsTime;

      std::mutex m_dataReqMutex;
      std::mutex m_clearReqMutex;

      std::atomic<unsigned long> m_clearRequestsReceived;
      std::atomic<unsigned long> m_requestsReceived;
      std::atomic<unsigned long> m_lastClearXId;
      std::atomic<unsigned long> m_totalRobsRequested;
      bool m_ignoreClearXId;

      std::vector<NPReadoutModule*> m_readoutModules;

      tbb::concurrent_bounded_queue<NPRequestDescriptor*>* m_collectorQueue;

#ifdef MONITORING
      monsvc::MonitoringService& m_monitoringService;
      monsvc::ptr<TH1D> m_timeHistogram;
      monsvc::ptr<TH1D> m_sizeHistogram;
      monsvc::ptr<TH1D> m_channelsHistogram;
#endif
   };



   class DescriptorDataServer: public DataServer {
   public:
      DescriptorDataServer(std::vector<boost::asio::io_service>& ioService,
                           const std::string& iomName,
                           daq::asyncmsg::NameService* nameService,
                           NPTrigger* trigger);
      virtual ~DescriptorDataServer();
   private:
      NPTrigger* m_trigger;
      virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session>) noexcept;
   };


   class DescriptorDataServerSession: public DataServerSession {

   public:
      DescriptorDataServerSession(boost::asio::io_service& ioService,

                                  NPTrigger* trigger);
      virtual ~DescriptorDataServerSession();
   private:
      NPTrigger* m_trigger;
      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override;
      virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>  message) noexcept override;
      virtual void onSendError(const boost::system::error_code& error,
                               std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override;
   };


   class DescriptorClearSession: public ClearSession {
   public:
      DescriptorClearSession(boost::asio::io_service& ioService,
                             const std::string& mcAddress,
                             const std::string& mcInterface,
                             NPTrigger* trigger):
         ClearSession(ioService, mcAddress, mcInterface),
         m_trigger(trigger) {};
      virtual ~DescriptorClearSession(){};
   private:
      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      NPTrigger* m_trigger;
   };

}
#endif
