// -*- c++ -*-
#ifndef REQUEST_DESCRIPTOR_H
#define REQUEST_DESCRIPTOR_H

#include <vector>
#include <chrono>
#include <memory>

namespace daq {
   namespace asyncmsg {
      class Session;
   }
}

namespace ROS {
   class RequestDescriptor {
   public:
      enum RequestStatus {NEW_REQUEST,
                          REQUEST_PENDING,
                          REQUEST_COMPLETE,
                          REQUEST_CORRUPT};

      RequestDescriptor();
      virtual ~RequestDescriptor();


      /**
       *  Since RequestDescriptors are re-used many times, all Request
       *  specific initialisation is done here rather than in the
       *  constructor
       **/
      virtual void initialise(unsigned int level1Id,
                              const std::vector<unsigned int>* channelList,
                              std::shared_ptr<daq::asyncmsg::Session> destination,
                              unsigned int transactionId)=0;


      /**
       * Return the L1 id of the current request
       **/
      unsigned int level1Id();

      /**
       * Send the data to the requester
       **/
      virtual void send()=0;

      std::chrono::time_point<std::chrono::system_clock> requestTime();

   protected:
      unsigned int m_transactionId;
      unsigned int m_level1Id;
      std::weak_ptr<daq::asyncmsg::Session> m_destination;
      /**
       *  Timestamp of when this descruptor was initialised. Used to
       *  check for timeouts.
       **/
      std::chrono::time_point<std::chrono::system_clock> m_initialiseTime;
   };

   inline unsigned int RequestDescriptor::level1Id() {
      return m_level1Id;
   }

   inline std::chrono::time_point<std::chrono::system_clock>
   RequestDescriptor::requestTime() {
      return m_initialiseTime;
   }

}

#endif
