// -*- c++ -*-
#ifndef NPREADOUT_MODULE_H
#define NPREADOUT_MODULE_H

#include <vector>
#include "ROSCore/ReadoutModule.h"

#include "tbb/concurrent_queue.h"

namespace ROS {
   class RequestDescriptor;
   class NPRequestDescriptor;
   class DataPage;
   class NPReadoutModule : public ReadoutModule {

   public:
      virtual void setCollectorQueue(
         tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue);

      virtual void registerChannels(unsigned int&,
                                    std::vector<tbb::concurrent_bounded_queue<DataPage*>* >&,
                                    std::vector<std::vector<uint32_t> >&)=0;

      virtual void getL1CountPointers(std::vector<unsigned int*>&,std::vector<bool*>&)=0;

      virtual void requestFragment(NPRequestDescriptor* descriptor)=0;

      virtual void clearFragments(std::vector<uint32_t>& level1Ids)=0;
   protected:
      tbb::concurrent_bounded_queue<NPRequestDescriptor*>* m_collectorQueue;

   };

   inline void NPReadoutModule::setCollectorQueue(
      tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue) {
      m_collectorQueue=descriptorQueue;
   }
}

#endif
