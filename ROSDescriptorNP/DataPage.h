// -*- c++ -*-

#ifndef DATA_PAGE_H
#define DATA_PAGE_H

namespace ROS{
   class DataPage {
   public:
      DataPage(unsigned int* virtAddr, unsigned int* physAddr);
      unsigned int* physicalAddress();
      unsigned int* virtualAddress();
      void  virtualAddress(unsigned int* nv);
   private:
      unsigned int* m_virtualAddress;
      unsigned int* m_physicalAddress;
   };


   inline DataPage::DataPage(unsigned int* virtAddr, unsigned int* physAddr)
      : m_virtualAddress(virtAddr), m_physicalAddress(physAddr) {
   }


   inline unsigned int* DataPage::physicalAddress() {
      return m_physicalAddress;
   }

   inline unsigned int* DataPage::virtualAddress() {
      return m_virtualAddress;
   }
   inline void DataPage::virtualAddress(unsigned int* nv) {
      m_virtualAddress=nv;
   }
}
#endif
