// -*- c++ -*-

#ifndef NPREQUESTDESCRIPTOR
#define NPREQUESTDESCRIPTOR

#include <vector>
#include <map>
#include <chrono>
#include <mutex>
#include <iostream>
#include <memory>

#include "ROSDescriptorNP/RequestDescriptor.h"
#include "ROSDescriptorNP/DataPage.h"

#include "tbb/concurrent_queue.h"

namespace daq {
   namespace asyncmsg {
      class Session;
   }
}

namespace ROS {
   class NPReadoutModule;

   //!  RobinNP request descriptor class.
   class NPRequestDescriptor : public RequestDescriptor {
   public:
      virtual ~NPRequestDescriptor();

      static void configure(std::vector<NPReadoutModule*> readoutModules,
                            unsigned int nDescriptors);

      static void unconfigure();

      static unsigned int nChannels();

      static NPRequestDescriptor* getDescriptor(unsigned int id);
      static NPRequestDescriptor* getDescriptor();

      static unsigned int descriptorsCreated();
      static unsigned int descriptorsFree();
      uint32_t getId();


      /**
       *  Since RequestDescriptors are re-used many times, all Request
       *  specific initialisation is done here rather than in the
       *  constructor
       **/
      void initialise(unsigned int level1Id,
                      const std::vector<unsigned int>* channelList,
                      std::shared_ptr<daq::asyncmsg::Session> destination,
                      unsigned int transactionId) override;

      std::vector<unsigned int>* headerPage(unsigned int channelIndex);



      /**
       *  How many times has this descriptor been submitted since
       *  initialise()
       **/
      unsigned int tries();

      void dump();

      /**
       *  Get the DataPage associated with this channel.
       **/
      DataPage* dataPage(unsigned int channelIndex);

      std::vector<unsigned int>* localIds();

      unsigned int nChannelsRequested();

      RequestStatus status(unsigned int channelIndex);
      /**
       *  Set the status of the request w.r.t. the given channel
       **/
      void status(unsigned int channelIndex, RequestStatus);


      unsigned int size();


      void freeResources();

      std::vector<unsigned int>* header(unsigned int channelIndex);

      bool complete();

      /**
       * Did any of the data channels in the request return a "May
       * come" status?
       **/
      bool channelsPending();

      /**
       * Did any of the data channels in the request return a corrupt
       * fragment status?
       **/
      bool channelsCorrupt();


      void submitted(bool);
      void expectReply(int nExpected=1);
      void gotReply();


      /**
       * Send the data to the requester
       **/
      void send() override;

      void assignPage(unsigned int channelIndex, DataPage* page);
   private:
      NPRequestDescriptor();


   protected:

//       std::vector<unsigned int> m_channelList;
      std::vector<unsigned int> m_localList;
      std::vector<RequestStatus> m_fragmentStatus;

      /**
       *  A DataPage will be allocated for each DataChannel during a request
       **/
      std::vector<DataPage*> m_dataPage;

      /**
       * Header pages are allocated in the constructor
       **/
      std::vector<std::vector<unsigned int>* > m_headerPage;


      unsigned int m_nChannels;

      /**
       *  Count of the number of channels that have received a reply
       *  from the robin
       **/
      unsigned int m_nRepliesReceived;


      /**
       *  How many times this request has been tried.
       **/
      unsigned int m_tries;


      /**
       * Identifier of this descriptor (index into static vector below)
       **/
      uint32_t m_id;
      bool m_submitted;
      unsigned int m_repliesExpected;


      static uint32_t s_nextId;
      static std::vector<NPRequestDescriptor*> s_descriptorVector;
      static uint32_t s_nDescriptors;
      static tbb::concurrent_bounded_queue<unsigned int> s_freeQueue;

      static std::vector<tbb::concurrent_bounded_queue<DataPage*>* > s_pageQueue;
      static std::vector<std::vector<uint32_t> > s_channelVec;
      static std::map<unsigned int, unsigned int> s_channelMap;
      static std::vector<unsigned int> s_subrobMap;
      static std::vector<uint32_t> s_idMap;
      static std::mutex s_mutex;
   };

   inline unsigned int NPRequestDescriptor::nChannels() {
      return s_channelMap.size();
   }
   inline uint32_t NPRequestDescriptor::getId() {
      return m_id;
   }

   inline NPRequestDescriptor* NPRequestDescriptor::getDescriptor(unsigned int id) {
      // Protect concurrect access to s_nextId and
      // s_descriptorVector
      std::lock_guard<std::mutex> lock(NPRequestDescriptor::s_mutex);
      if (id<s_nextId) {
	 return s_descriptorVector[id];
      }
      else {
         std::cerr << "Invalid descruptor ID " << id << std::endl;
         return 0;
      }
   }

   inline NPRequestDescriptor* NPRequestDescriptor::getDescriptor() {
      unsigned int id;
      if (s_freeQueue.try_pop(id)) {
	 // Protect concurrect access to s_descriptorVector
	 std::lock_guard<std::mutex> lock(NPRequestDescriptor::s_mutex);
         return s_descriptorVector[id];
      }
      else {
         return new NPRequestDescriptor;
      }
   }

   inline unsigned int NPRequestDescriptor::descriptorsCreated() {
      return s_nextId;
   }
   inline unsigned int NPRequestDescriptor::descriptorsFree() {
      // tbb queue may return a negative size under
      // certain conditions
      auto n = s_freeQueue.size();
      return n > 0 ? n : 0;
   }


   inline unsigned int NPRequestDescriptor::nChannelsRequested(){
      return m_nChannels;
   }

   inline unsigned int NPRequestDescriptor::tries() {
      return m_tries;
   }


   inline NPRequestDescriptor::RequestStatus NPRequestDescriptor::status(unsigned int channelIndex) {
      return m_fragmentStatus[channelIndex];
   }
   inline void NPRequestDescriptor::status(unsigned int channelIndex,
                                           NPRequestDescriptor::RequestStatus status) {
      m_fragmentStatus[channelIndex]=status;
   }

   inline void NPRequestDescriptor::submitted(bool flag) {
      m_submitted=flag;
   }


   inline std::vector<unsigned int>* NPRequestDescriptor::headerPage(
      unsigned int channelIndex) {
      return m_headerPage[channelIndex];
   }


   inline void NPRequestDescriptor::expectReply(int nExpected) {
      m_repliesExpected+=nExpected;
   }

   inline void NPRequestDescriptor::gotReply() {
      m_nRepliesReceived++;
   }

   inline void NPRequestDescriptor::assignPage(unsigned int channelIndex,
                                               DataPage* page) {
      m_dataPage[channelIndex]=page;
   }
   inline DataPage* NPRequestDescriptor::dataPage(unsigned int channelIndex) {
      return m_dataPage[channelIndex];
   }

   inline std::vector<unsigned int>* NPRequestDescriptor::localIds(){
      return &m_localList;
   }

}
#endif
