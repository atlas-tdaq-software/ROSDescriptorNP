// -*- c++ -*-
// $Id: $ 

#ifndef DESCRIPTORNPEXCEPTION_H
#define DESCRIPTORNPEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>

class DescriptorNPException : public ROSException {

public:
   enum ErrorCode {
                   BAD_CHANNEL_ID,
                   TIMEOUT
   };

   explicit DescriptorNPException(ErrorCode error) ;
   DescriptorNPException(ErrorCode error, const std::string& description) ;
   DescriptorNPException(ErrorCode error, const ers::Context& context) ;
   DescriptorNPException(ErrorCode error, const std::string& description,
                         const ers::Context& context) ;
   DescriptorNPException(const std::exception& cause, ErrorCode error,
                         const std::string& description,
                         const ers::Context& context);

protected:
   virtual ers::Issue * clone() const override{
      return new DescriptorNPException( *this ); }
   static const char * get_uid() { return "ROS::DescriptorNPException"; }
   virtual const char* get_class_name() const override {return get_uid();}
   std::string getErrorString(unsigned int errorId) const;

};

inline
DescriptorNPException::DescriptorNPException(DescriptorNPException::ErrorCode error) 
   : ROSException("DescriptorPackage",error,getErrorString(error)) { }

inline
DescriptorNPException::DescriptorNPException(DescriptorNPException::ErrorCode error,
                                             const std::string& description) 
   : ROSException("DescriptorPackage",error,getErrorString(error),description) { }
inline
DescriptorNPException::DescriptorNPException(DescriptorNPException::ErrorCode error, const ers::Context& context) 
   : ROSException("DescriptorPackage",error,getErrorString(error),context) { }

inline
DescriptorNPException::DescriptorNPException(DescriptorNPException::ErrorCode error,
                                             const std::string& description,
                                             const ers::Context& context) 
   : ROSException("DescriptorPackage",error,getErrorString(error),description,context) { }

inline DescriptorNPException::DescriptorNPException(const std::exception& cause,
                                                    ErrorCode error,
                                                    const std::string& description,
                                                    const ers::Context& context) 
     : ROSException(cause, "DescriptorPackage", error, getErrorString(error), description, context) {}

inline std::string DescriptorNPException::getErrorString(unsigned int errorId) const
{
	std::string result;    
   switch (errorId) {
   case BAD_CHANNEL_ID:
      result="Bad channel ID";
      break;
   case TIMEOUT:
      result="Timeout";
      break;
   default:
      result = "Unspecified error";
      break;
   }
   return(result);
}


#endif
