// -*- c++ -*-
#ifndef REQUESTMANAGER_H
#define REQUESTMANAGER_H

#include <cstdint>
#include <vector>
#include <thread>
#include <mutex>
#include <set>
#include <condition_variable>
#include <chrono>
#include <atomic>

#ifdef MONITORING
# include "monsvc/ptr.h"
#endif
class TH1F;

namespace ROS{
   class RequestDescriptor;
   //! RequestManager base class.
   class RequestManager{
   public:
      RequestManager(unsigned int, unsigned int, unsigned int,
                     unsigned int maxPendigClears=20000);
      virtual ~RequestManager();

      virtual void start();
      virtual void stop();

      virtual void addRequest(RequestDescriptor*);
      void addClears(std::vector<unsigned int>& level1IdList);

      unsigned int pending()const {return m_requests.size();};
      unsigned int timedOut()const {return m_nTimedOut;};
      unsigned int clearsPending()const {return m_pendingClears.size();};
      uint64_t lateRequests()const {return m_lateRequests;};
      uint64_t lateClears()const {return m_lateClears;};
      uint64_t clearsTimedOut()const {return m_timedOutclears;};
      virtual void dump();

      enum l1IdEnum {L1_MIN_WRAP=0x08000000,
                     L1_MAX_WRAP=0xf8000000,
                     L1_INITIAL=0xffffffff};
      static unsigned int highestValue(const std::vector<unsigned int>& l1Vec);
      virtual std::vector<unsigned int> pollLevel1()=0;

      bool checkAvailable(unsigned int l1Id);
   protected:
      class Request {
      public:
         Request(unsigned int l1, RequestDescriptor* desc)
            :m_l1Id(l1),
             m_descriptor(desc){};
         bool operator< (const Request& other) const {
            return m_l1Id<other.m_l1Id;};
         RequestDescriptor* descriptor() const {return m_descriptor;};
      private:
         unsigned int m_l1Id;
         RequestDescriptor* m_descriptor;
      };

      class Clear {
      public:
         Clear(unsigned int l1,std::chrono::system_clock::time_point timeStamp) : m_l1Id(l1),m_timeStamp(timeStamp){};
         bool operator< (const Clear& other) const {
            return m_l1Id<other.m_l1Id;};
      //private:
         unsigned int m_l1Id;
         std::chrono::system_clock::time_point m_timeStamp;
      };

      virtual void doRequest(RequestDescriptor* desc)=0;
      virtual void doDeferredRequest(RequestDescriptor* desc);
      virtual void doClears(std::vector<unsigned int>& level1IdList)=0;
      void pendingHandler();

      std::atomic<unsigned int> m_availableL1;

      uint64_t m_lateClears;
      uint64_t m_lateRequests;
      uint64_t m_timedOutclears;
      std::set<Request> m_requests;
      //std::set<unsigned int> m_pendingClears;
      std::set<Clear> m_pendingClears;
      unsigned int m_nTimedOut;
      unsigned int m_nDiscards;
      unsigned int m_requestTimeOut;
      unsigned int m_conditionTimeout;
      unsigned int m_clearTimeout;
      unsigned int m_maxPendingClears;

      std::mutex m_mutex;
      std::mutex m_requestMutex;
      std::mutex m_clearMutex;
      std::condition_variable m_condition;

      bool m_runActive;
      std::thread* m_pendingThread;

#ifdef MONITORING
      monsvc::ptr<TH1F> m_requestDelayHistogram;
      monsvc::ptr<TH1F> m_clearDelayHistogram;
#endif
   };
   inline void RequestManager::doDeferredRequest(RequestDescriptor* desc){
      doRequest(desc);
   }
}
#endif
